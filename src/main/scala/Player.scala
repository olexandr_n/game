import math._
import scala.util._

/**
  * Auto-generated code below aims at helping you parse
  * the standard input according to the problem statement.
  **/
object Player extends App {
  val factorycount = readInt // the number of factories
  val linkcount = readInt // the number of links between factories
  var ls: List[Array[Int]] = Nil
  for(i <- 0 until linkcount) yield {
    val Array(factory1, factory2, distance) = for(i <- readLine split " ") yield i.toInt
    ls :+= Array(factory1, factory2, distance)
  }

  while(true) {
    var ls_f_m: List[Array[Int]]=Nil  //список моїх фабрик
    var ls_f_e: List[Array[Int]]=Nil  //список фабрик ворога
    var ls_f_n: List[Array[Int]]=Nil  //спсок нейтральних фабрик
    var ls_w_m: List[Array[Int]]=Nil  //список моїх військ
    var ls_w_e: List[Array[Int]]=Nil  //список військ ворога


    val entitycount = readInt // the number of entities (e.g. factories and troops)
    for(i <- 0 until entitycount) {
      val Array(_entityid, entitytype, _arg1, _arg2, _arg3, _arg4, _arg5) = readLine split " "

      val entityid = _entityid.toInt
      val arg1 = _arg1.toInt
      val arg2 = _arg2.toInt
      val arg3 = _arg3.toInt
      val arg4 = _arg4.toInt
      val arg5 = _arg5.toInt

      // фільтрація даних по списках
      val arr = Array(entityid, arg1, arg2, arg3, arg4, arg5)
      entitytype match {
        case "FACTORY" => arg1 match {
          case 1 => ls_f_m :+= arr
          case -1 => ls_f_e :+= arr
          case _ => ls_f_n :+= arr
        }
        case "TROOP" => arg1 match {
          case 1 => ls_w_m :+= arr
          case -1 => ls_w_e :+= arr
        }
      }
    }

    def attak(i: Array[Int],mishen: Array[Int], index: Int): Unit = {
      println("MOVE " + i(0) + " " + mishen(0) + " "+ i(2) / index)

    }

    val maxA = ls_f_m.foldLeft(ls_f_m.head)((max,a) => if (max(3) < a(3)) a else max)
    if(!ls_f_n.isEmpty){
      val max_n = ls_f_n.sortWith((a,b) => a(3) > b(3)).take(4)
      val max_m = ls_f_m.sortWith((a,b) => a(2) < b(2)).take(3)

      for (v <- max_n)
        for (r <- max_m)
          attak(r, v, 2)
      println("WAiT")
    }else{
      val max_e = ls_f_e.sortWith((a,b) => a(2) > b(2)).take(4)
      val max_m = ls_f_m.sortWith((a,b) => a(2) < b(2)).take(3)

      for (v <- max_e)
        for (r <- max_m)
          attak(r, v,2)

      println("WAiT")
    }
  }
}