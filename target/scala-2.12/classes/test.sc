val factorycount = 0 // the number of factories
val linkcount = 1 // the number of links between factories
val ls = for (i <- 0 until linkcount) yield {
  val  Array(factory1, factory2, distance) = for (i <- "3 2 5"split " ") yield i.toInt
  Array(factory1, factory2, distance)
}
ls